//https://api.openweathermap.org/data/2.5/forecast?q=Boston&units=metric&appid=693ccd9d9883d0124af7db68de1a14a2

const key = "693ccd9d9883d0124af7db68de1a14a2";
const getForecast = async (city) => {
    const base = "https://api.openweathermap.org/data/2.5/forecast"
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base+query);
    if(!response.ok)
        throw new Error("Status code "+ response.status);
    
    const data = await response.json();
    return data;
    
}

getForecast('Gujarat')
.then(data=>console.log(data))
.catch(err=>console.warn(err));